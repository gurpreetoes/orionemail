<?php  
   session_start();
   $_SESSION['username'];
 
   if(empty($_SESSION['username']))
  { 
   header("location:index.php");
  } ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Orion eSOlutions :: Thanks </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util2.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body cz-shortcut-listen="true">
	<?php include("include/header.php"); ?>
	<div class="limiter">
		<div class="container-thanks100">
			<div class="wrap-mail100 p-l-26 p-r-26 p-b-26">	
				<form action="" method="post" class="login100-form validate-form">
				<div class="p-r-55 p-b-24">
					<span class="login100-form-title p-b-26">
						Send Email
					</span>
					<div class="alert alert-success" role="alert">
						<?php
							echo $status=$_GET['status'];
								
						?>
					</div>
					</div>
				</form>
				</div>
			</div>
		</div>
			<?php include("include/footer.php"); ?>
</body>
</html>
