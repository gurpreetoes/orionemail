-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 07, 2019 at 09:04 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orion_mail`
--

-- --------------------------------------------------------

--
-- Table structure for table `emailtemplate`
--

DROP TABLE IF EXISTS `emailtemplate`;
CREATE TABLE IF NOT EXISTS `emailtemplate` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailtemplate`
--

INSERT INTO `emailtemplate` (`id`, `subject`, `template`) VALUES
(3, 'Diwali Holiday', '<p>Hello Sir,</p>\r\n\r\n<p>we are on leave due to diwali function,so please manage accordingly.</p>\r\n\r\n<p>Happy Diwali&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://cdn-ap-ec.yottaa.net/56213828312e5803810043dd/www.hallmark.com/v~4b.1ea/dw/image/v2/AALB_PRD/on/demandware.static/-/Sites-hallmark-master/default/dwf2130862/images/finished-goods/Flowers-and-Diyas-PopUp-Diwali-Card-root-399DIW3355_DIW3355_1470_1.jpg_Source_Image.jpg?sw=1024&amp;yocs=27_&amp;yoloc=ap\" style=\"height:500px; width:500px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Thanks!</p>\r\n\r\n<p>Orion eSolutions</p>\r\n\r\n<p>&nbsp;</p>\r\n'),
(4, 'dusshera Wishes', '<p><strong>Hello Dear,</strong></p>\r\n\r\n<p><strong>OrionE solutions Wishing you Happy Dussehra.</strong></p>\r\n\r\n<p><strong><img alt=\"dusshera\" src=\"https://www.hindutsav.com/wp-content/uploads/2015/09/dussehra-image.jpg\" style=\"height:338px; width:600px\" /></strong></p>\r\n\r\n<p><strong>Enjoy the day</strong></p>\r\n\r\n<p><strong>Thanks.</strong></p>\r\n'),
(9, 'Test template', '<p><strong>Dear Sir,</strong></p>\r\n\r\n<p>Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;</p>\r\n\r\n<p>Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;Tested tested&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Thanks!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
